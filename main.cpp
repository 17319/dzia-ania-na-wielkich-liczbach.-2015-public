#include <iostream>
#include <cstring>
#include <string>
#include <stdlib.h>
#include <sstream>

using namespace std;

struct TlistElement
{
    TlistElement * next;
    short key;
};
class TsingleList
{
private:
    TlistElement * front, * back;
    unsigned long long counter;
public:
    TsingleList()
    {
        front = back = NULL;
        counter  = 0;
    }
    ~TsingleList()
    {
        TlistElement * p;

        while(front)
        {
            p = front->next;
            delete front;
            front = p;
        }
    }
    unsigned long long size()
    {
        return counter;
    }
    TlistElement * push_front(TlistElement * p)
    {
        p->next = front;
        front = p;
        if(!back) back = front;
        counter++;
        return front;
    }
    TlistElement * push_back(TlistElement * p)
    {
        if(back) back->next = p;
        p->next = NULL;
        back = p;
        if(!front) front = back;
        counter++;
        return back;
    }
    TlistElement * insert(TlistElement * p1, TlistElement * p2)
    {
        p1->next = p2->next;
        p2->next = p1;
        if(!(p1->next)) back = p1;
        counter++;
        return p1;
    }
    TlistElement * pop_front()
    {
        TlistElement * p;

        if(front)
        {
            p = front;
            front = front->next;
            if(!front) back = NULL;
            counter--;
            return p;
        }
        else return NULL;
    }
    TlistElement * pop_back()
    {
        TlistElement * p;
        if(back)
        {
            p = back;
            if(p == front) front = back = NULL;
            else
            {
                back = front;
                while(back->next != p) back = back->next;
                back->next = NULL;
            }
            counter--;
            return p;
        }
        else return NULL;
    }
    TlistElement * erase(TlistElement * p)
    {
        TlistElement * p1;

        if(p == front) return pop_front();
        else
        {
            p1 = front;
            while(p1->next != p) p1 = p1->next;
            p1->next = p->next;
            if(!(p1->next)) back = p1;
            counter--;
            return p;
        }
    }
    TlistElement * index(unsigned n)
    {
        TlistElement * p;
        if((!n) || (n > counter)) return NULL;
        else if(n == counter) return back;
        else
        {
            p = front;
            while(--n) p = p->next;
            return p;
        }
    }
    void showKeys()
    {
        TlistElement * p;
        if(!front) cout << "0" << endl;
        else
        {
            p = front;
            while(p)
            {
                cout << p->key << "";
                p = p->next;
            }
            cout << endl;
        }
    }
};

bool czy_2_w(string a, string b)
{
    bool czy_druga_wieksza = false;
    for (unsigned long long i =0; i<a.length(); i++)
    {
        if (b[i]<a[i]) break;
        else if (b[i]>a[i])
        {
            czy_druga_wieksza = true;
            break;
        }
    }
    return czy_2_w;
}

unsigned long long str2int(string str)
{
    stringstream ss;
    unsigned long long temp;
    ss << str;
    ss >> temp;
    return temp;
}

TsingleList dodwanie_list(TsingleList l1, TsingleList l2)
{
    TsingleList l3;
    unsigned long long  i = l1.size(); // dlugosc liczby a
    unsigned long long j = l2.size(); // dlugosc liczby b
    unsigned long long  n = i;
    if(j < i) n = j; // dlugosc krótszej liczby
    unsigned long long cyfra = 0;
    unsigned long long k,w;
    TlistElement * p;
    for(k = 1; k <= n; k++)
    {
        w = l1.index(i--)->key + l2.index(j--)->key + cyfra;
        cyfra  = w / 10;
        p = new TlistElement;
        p->key=(w%10);
        l3.push_front(p);
        p=p->next;
    }
    while(i)
    {
        w  = l1.index(i--)->key + cyfra;
        cyfra  = w / 10;
        p = new TlistElement;
        p->key=(w%10);
        l3.push_front(p);
        p=p->next;
    }
    while(j)
    {
        w  = l2.index(j--)->key + cyfra;
        cyfra  = w / 10;
        p = new TlistElement;
        p->key=(w%10);
        l3.push_front(p);
        p=p->next;
    }
    if (cyfra)
    {
        p = new TlistElement;
        p->key=(cyfra);
        l3.push_front(p);
    }
    return l3;
}

TsingleList kopiuj_liste(TsingleList l)
{
    TsingleList lista;
    TlistElement * p;
    for (unsigned long long i=1; i<=l.size(); i++)
    {
        p = new TlistElement;
        p->key=(l.index(i)->key);
        lista.push_back(p);
    }
    return lista;
}

string str_cpy(TsingleList l)
{
    TsingleList lista;
    string a = "";
    for (unsigned long long i=1; i<=l.size(); i++)
        a=a+'a';
    for (unsigned long long i=1; i<=l.size(); i++)
    {
        a[i-1]=l.index(i)->key+48;
    }
    return a;
}

TsingleList dodawanie(string a, string b)
{
    TsingleList    l1,l2,l3;
    TlistElement * p;
    for (unsigned long long i=0; i<a.length(); i++)
    {
        p = new TlistElement;
        p->key=(a[i]-48);
        l1.push_back(p);
    }
    for (unsigned long long i=0; i<b.length(); i++)
    {
        p = new TlistElement;
        p->key=(b[i]-48);
        l2.push_back(p);
    }
    unsigned long long  i = l1.size();
    unsigned long long j = l2.size();
    unsigned long long  n = i;
    if(j < i) n = j;
    unsigned long long cyfra = 0;
    unsigned long long k,w;
    for(k = 1; k <= n; k++)
    {
        w = l1.index(i--)->key + l2.index(j--)->key + cyfra;
        cyfra  = w / 10;
        p = new TlistElement;
        p->key=(w%10);
        l3.push_front(p);
        p=p->next;
    }
    while(i)
    {
        w  = l1.index(i--)->key + cyfra;
        cyfra  = w / 10;
        p = new TlistElement;
        p->key=(w%10);
        l3.push_front(p);
        p=p->next;
    }
    while(j)
    {
        w  = l2.index(j--)->key + cyfra;
        cyfra  = w / 10;
        p = new TlistElement;
        p->key=(w%10);
        l3.push_front(p);
        p=p->next;
    }
    if (cyfra)
    {
        p = new TlistElement;
        p->key=(cyfra);
        l3.push_front(p);
    }
    return l3;
}

TsingleList odejmowanie(string a, string b)
{
    while (a.length()>b.length())
    {
        b='0'+b;
    }
    while (a.length()<b.length())
    {
        a='0'+a;
    }

    string c;
    if (a.length()<b.length())
    {
        c=a;
        a=b;
        b=c;
    }
    TsingleList    l1,l2,l3;
    TlistElement * p;
    for (unsigned long long i=0; i<a.length(); i++)
    {
        p = new TlistElement;
        p->key=(a[i]-48);
        l1.push_back(p);
    }

    for (unsigned long long i=0; i<b.length(); i++)
    {
        p = new TlistElement;
        p->key=(b[i]-48);
        l2.push_back(p);
    }
    unsigned long long  i = l1.size();
    unsigned long long j = l2.size();
    unsigned long long  n = i;
    if(j < i) n = j;
    unsigned long long cyfra = 0;
    unsigned long long k,w;
    for(k = 1; k <= n; k++)
    {
        unsigned long long ii = i--;
        unsigned long long jj = j--;
        if (l1.index(ii)->key >= l2.index(jj)->key)
        {
            w = l1.index(ii)->key - l2.index(jj)->key;
            p = new TlistElement;
            p->key=(w);
            l3.push_front(p);
            p=p->next;
        }
        else
        {
            l1.index(ii-1)->key=(l1.index(ii-1)->key)-1;
            l1.index(ii)->key = (l1.index(ii)->key ) + 10;
            w = l1.index(ii)->key - l2.index(jj)->key;
            p = new TlistElement;
            p->key=(w);
            l3.push_front(p);
            p=p->next;
        }
    }
    while(i)
    {
        w  = l1.index(i--)->key + cyfra;
        cyfra  = w / 10;
        p = new TlistElement;
        p->key=(w%10);
        l3.push_front(p);
        p=p->next;
    }
    while(j)
    {
        w  = l2.index(j--)->key;
        p = new TlistElement;
        p->key=(w);
        l3.push_front(p);
        p=p->next;
    }
    if (cyfra)
    {
        p = new TlistElement;
        p->key=(w%10);
        l3.push_front(p);

    }

    for (unsigned long long i = 1; i<=l3.size(); i++)
    {
        if (l3.index(i)->key == 0)
        {
            l3.erase(l3.index(i));
            i--;
        }
        else
            goto a2;
    }
a2:
    return l3;
}

TsingleList mnozenie(string a, string b)
{
    string druga_odp;
    TsingleList    l1,l2,l3;
    TlistElement * p;
    for (unsigned long long i=0; i<a.length(); i++)
    {
        p = new TlistElement;
        p->key=(a[i]-48);
        l1.push_back(p);
    }
    for (unsigned long long i=0; i<b.length(); i++)
    {
        p = new TlistElement;
        p->key=(b[i]-48);
        l2.push_back(p);
    }
    for (unsigned long long i=1; i<=b.length(); i++)
    {
        unsigned long long razy = l2.pop_back()->key;
        string odp="";
        for (unsigned long long iii=1; iii<=razy; iii++)
        {
            if (iii==1)
                odp = a;
            if (iii>1)
                odp = str_cpy(dodawanie(a,odp));
        }
        odp = str_cpy(dodawanie(odp,"0"));
        if (b.length()==1)
        {
            druga_odp=odp;
        }
        else
        {
            for (unsigned long long k=1; k<i; k++)
                odp=odp+'0';
            druga_odp=str_cpy(dodawanie(druga_odp,odp));
        }
    }
    for (unsigned long long i=0; i<druga_odp.length(); i++)
    {
        p = new TlistElement;
        p->key=(druga_odp[i]-48);
        l3.push_back(p);
    }
    return l3;
}

TsingleList dzielenie_mod (string liczba, unsigned long long dzielnik)
{
    TsingleList    l1,l2,l3;
    TlistElement * p;
    unsigned long long q=0;

    for (unsigned long long i=0; i<liczba.length(); i++)
    {
        p = new TlistElement;
        p->key=(liczba[i]-48);
        l1.push_back(p);
    }
    stringstream ss;
    string dzielnik_s;
    ss << dzielnik;
    ss >> dzielnik_s;
    for (unsigned long long i=0; i<dzielnik_s.length(); i++)
    {
        p = new TlistElement;
        p->key=(dzielnik_s[i]-48);
        l2.push_back(p);
    }
    unsigned long long wynik = 0;
    for (unsigned long long i = 0; i < liczba.length(); i++)
    {
        wynik = wynik*10 + liczba[i]-'0';
        wynik = wynik%dzielnik;
    }
    stringstream sss;
    string wynik_s;
    sss << wynik;
    sss >> wynik_s;
    for (unsigned long long i=0; i<wynik_s.length(); i++)
    {
        p = new TlistElement;
        p->key=(wynik_s[i]-48);
        l3.push_back(p);
    }
    return l3;
}

TsingleList dziel (string liczba, string dzielnik)
{
    string n, p, q, r;
    TsingleList    l1,l2,l3;
    TlistElement * pp;
   // unsigned long long q=0;

    for (unsigned long long i=0; i<liczba.length(); i++)
    {
        pp = new TlistElement;
        pp->key=(liczba[i]-48);
        l1.push_back(pp);
    }
        for (unsigned long long i=0; i<dzielnik.length(); i++)
    {
        pp = new TlistElement;
        pp->key=(dzielnik[i]-48);
        l1.push_back(pp);
    }
    n = liczba;
    p = dzielnik;

    q = "0";
    r = n;
    while (str2int(r)>str2int(p))
    {
        q = str_cpy(dodawanie(q,"1"));
        r = str_cpy(odejmowanie(r,p));
    }
    if (str_cpy(dzielenie_mod(liczba,str2int(dzielnik)))=="0")
    {
        q = str_cpy(dodawanie(q,"1"));
    }
    for (unsigned long long i=0; i<q.length(); i++)
    {
        pp = new TlistElement;
        pp->key=(q[i]-48);
        l3.push_back(pp);
    }
return l3;
}





void kalkulator()
{
    TsingleList l1,l2,l3;
    string a, b,c;

    char  z1=1,z2=1,dzialanie;

    while(true)
    {
        cout << "Jakie dzialanie chcesz wykonac?\n"
             "dodowanie ==>>        wybierz d lub +\n"
             "odejmowanie ==>>      wybierz o lub -\n"
             "mnozenie ==>>         wybierz m lub *\n"
             "dzielenie_mod ==>>    wybierz x lub %\n"
             "dzielenie ==>>        wybierz y lub /\n"
             "Wybierz:\n";
        cin >> dzialanie;
        cout << endl;
        cout << "Pierwsza liczba w formacie znak liczba enter:     ";
        cin >> z1;
        cin >> a;
        cout << "Druga liczba w formacie znak liczba enter:        ";
        cin >> z2;
        cin >> b;
        cout << "########################## WYNIK ###############################\n\n";
        if (dzialanie=='+') dzialanie='d';
        if (dzialanie=='-') dzialanie='o';
        if (dzialanie=='*') dzialanie='m';
        if (dzialanie=='%') dzialanie='x';
        if (dzialanie=='/') dzialanie='y';
        if (dzialanie=='d') cout << "Suma podanych liczb wynosi: ";
        if (dzialanie=='o') cout << "Roznica podanych liczb wynosi: ";
        if (dzialanie=='m') cout << "Iloczyn podanych liczb wynosi: ";
        if (dzialanie=='x') cout << "Reszta z dzielenia tych liczb wynosi: ";
        if (dzialanie=='y') cout << "Iloraz podanych liczb wynosi: ";
        if (dzialanie=='m' && (a.length()<b.length()))
        {
            string c;
            c = a;
            a = b;
            b = c;
        }

        while (a.length()>b.length())
        {
            b='0'+b;
        }
        while (a.length()<b.length())
        {
            a='0'+a;
        }
        bool czy_druga_wieksza = false;
        for (unsigned long long i =0; i<a.length(); i++)
        {
            if (b[i]<a[i]) break;
            else if (b[i]>a[i])
            {
                czy_druga_wieksza = true;
                break;
            }
        }
        if (dzialanie == 'd' && (z1=='+' && z2=='+')) //OK
        {
            dodawanie(a,b).showKeys();
        }
        if (dzialanie == 'd' && (z1=='-' && z2=='-')) //OK
        {
            cout << "-";
            dodawanie(a,b).showKeys();
        }
        if (dzialanie == 'd' && (z1=='+' && z2=='-') && czy_druga_wieksza==true) //OK
        {
            cout << "-";
            odejmowanie(b,a).showKeys();//
        }
        if (dzialanie == 'd' && (z1=='+' && z2=='-') && czy_druga_wieksza==false) //OK
        {
            cout << "";
            odejmowanie(a,b).showKeys();
        }
        if (dzialanie == 'd' && (z1=='-' && z2=='+') && czy_druga_wieksza==false) //OK
        {
            if (a!=b)
                cout << "-";
            odejmowanie(a,b).showKeys();
        }
        if (dzialanie == 'd' && (z1=='-' && z2=='+') && czy_druga_wieksza==true) //OK
        {
            cout << "";
            odejmowanie(b,a).showKeys();
        }
        if (dzialanie== 'o' && czy_druga_wieksza==false && z1=='+'&&z2=='+') // OK
            odejmowanie(a,b).showKeys();

        if (dzialanie== 'o' && czy_druga_wieksza==true && z1=='+'&&z2=='+') //OK
        {
            cout << "-";
            odejmowanie(b,a).showKeys();
        }
        if (dzialanie== 'o'&& z1=='-'&&z2=='-' && czy_druga_wieksza==false) // OK
        {
            if (a!=b)
                cout << "-";
            odejmowanie(a,b).showKeys();
        }
        if (dzialanie== 'o'&& z1=='-'&&z2=='-' && czy_druga_wieksza==true) //OK
        {
            odejmowanie(b,a).showKeys();
        }
        if (dzialanie== 'o' && z1=='+'&&z2=='-') //OK
        {
            dodawanie(a,b).showKeys();
        }
        if (dzialanie== 'o' && z1=='-'&&z2=='+') //OK
        {
            cout << "-";
            dodawanie(a,b);
        }
        if (dzialanie=='m') //OK
        {
            if ((z1=='+'&&z2=='+') || (z1=='-'&&z2=='-'))
                mnozenie(a,b).showKeys();
            if ((z1=='+'&&z2=='-') || (z1=='-'&&z2=='+'))
            {
                cout << "-";
                mnozenie(a,b).showKeys();
            }
        }

        if (dzialanie=='x' || dzialanie == '%') //OK
        {
            if ((z1=='+'&&z2=='+') || (z1=='-'&&z2=='-'))
                dzielenie_mod(a,str2int(b)).showKeys();
        }

        if (dzialanie=='y') //OK
        {
            if ((z1=='+'&&z2=='+') || (z1=='-'&&z2=='-'))
                dziel(a,b).showKeys();
            if ((z1=='+'&&z2=='-') || (z1=='-'&&z2=='+'))
            {
                cout << "-";
                dziel(a,b).showKeys();
            }
        }

        cout << "\n########################## WYNIK ###############################\n";
        cout << "Czy chcesz kontynuowac? t/n : ";
        cin >> dzialanie;
        if (dzialanie=='t') system("cls");
        else break;
    }
}

int main()
{
    kalkulator();

    return 0;
}
